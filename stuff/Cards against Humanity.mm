<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1434109991067" ID="ID_339946216" MODIFIED="1434121731848" TEXT="Cards against Humanity">
<node CREATED="1434121740138" ID="ID_1460569080" MODIFIED="1434121841795" POSITION="right" TEXT="Rooms">
<node CREATED="1434121963881" ID="ID_422337801" MODIFIED="1434121966111" TEXT="Players">
<node CREATED="1434121842709" ID="ID_824068873" MODIFIED="1434121847502" TEXT="One admin">
<node CREATED="1434121848387" ID="ID_15501729" MODIFIED="1434122030434" TEXT="Can change pack selection, score and player limits, password, group name"/>
</node>
<node CREATED="1434122051683" ID="ID_792597543" MODIFIED="1434122059063" TEXT="At least two other players"/>
</node>
<node CREATED="1434121971592" ID="ID_342035388" MODIFIED="1434121971592" TEXT=""/>
</node>
<node CREATED="1434122145499" ID="ID_77284104" MODIFIED="1434122147373" POSITION="left" TEXT="Sessions">
<node CREATED="1434122175349" ID="ID_342091439" MODIFIED="1434150169274" TEXT="Users get a unique UUID, stored in a cookie. A hash signing the UUID with the IP and the user agent should also be stored. The hash is stored in a signed cookie, same with the UUID."/>
<node CREATED="1434122204606" ID="ID_59897152" MODIFIED="1434298931352" TEXT="Users are kicked if they are away for more than 15 seconds"/>
<node CREATED="1434122411392" ID="ID_121402942" MODIFIED="1434122411392" TEXT=""/>
</node>
</node>
</map>
