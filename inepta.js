/**
   .___                          __            .____                .___               
   |   |  ____    ____  ______ _/  |_ _____    |    |     __ __   __| _/__ __   _____  
   |   | /    \ _/ __ \ \____ \\   __\\__  \   |    |    |  |  \ / __ ||  |  \ /     \ 
   |   ||   |  \\  ___/ |  |_> >|  |   / __ \_ |    |___ |  |  // /_/ ||  |  /|  Y Y  \
   |___||___|  / \___  >|   __/ |__|  (____  / |_______ \|____/ \____ ||____/ |__|_|  /
             \/      \/ |__|               \/          \/            \/             \/ 
**/

// Constants
var secret = 'KaasIsBaas892P04xLf)kG_AQ0J<41BI/#p2wZ|';
var cardcastCacheFile = 'cardcast-cache.db';
var adminCredentials = {
	username: 'admin',
	password: 'a9cf9d08a8031b0a9d38e91d5d54f274e480a2f4'
};

// Libraries
var auth = require('basic-auth');
var cookie = require('cookie-signature');
var express = require('express');
var fs = require('fs');
var https = require('https');
var sha1 = require('sha1');
var sqlite3 = require('sqlite3');
var uglify = require('./middleware/express-uglify');
var uuid = require('uuid');

// Models
var Game = require('./models/game');
var User = require('./models/user');
var Collection = require('./models/collection');

// Instances
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var db;

var games = new Collection(Game);
var users = new Collection(User);

var defaultDecks;
var defaultDecksCodes;

// Middlewares
app
	.get('/decks/default', function(req, res) {
		res.json(defaultDecks);
	})
	.get(/^\/decks\/([A-Z0-9]{5})\/?$/, function(req, res) {
		if(defaultDecksCodes.indexOf(req.params[0]) > -1)
			return res.json({ error: "This Cardcast code belongs to one of the default card decks." });

		_getCardcastMetaData(req.params[0], function(err, deck) {
			if(err)
				res.json({ error: err.toString().substr(7) });
			else
				res.json(deck);
		});
	})
	.get(/^\/decks\/([A-Z0-9]{5})\/cards\/?$/, function(req, expRes) {
		db.get("SELECT json FROM cards WHERE code = ?", req.params[0], function(err, row) {
			if(err)
				expRes.json({ error: err.toString() });
			else if(row)
				expRes.type('json').send(row.json);
			else {
				https.get('https://api.cardcastgame.com/v1/decks/' + req.params[0] + '/calls', function(res) {
					res.setEncoding('utf8');
					var calls = '';
					res.on('data', function(chunk) {
						calls += chunk;
					});
					res.on('end', function() {
						if(calls) {
							https.get('https://api.cardcastgame.com/v1/decks/' + req.params[0] + '/responses', function(res) {
								res.setEncoding('utf8');
								var responses = '';
								res.on('data', function(chunk) {
									responses += chunk;
								});
								res.on('end', function() {
									if(responses) {
										var combined = '{"calls":' + calls.replace(/\{"id":"[^"]+","text":(.+?),"created_at":"[^"]+"}/g, '$1') + ',"responses":' + responses.replace(/\{"id":"[^"]+","text":\[(.+?)\],"created_at":"[^"]+"}/g, '$1') + '}';
										expRes.type('json').send(combined);
										db.run("INSERT INTO cards VALUES (?, ?, ?)", req.params[0], combined, Math.floor(Date.now() / 1000) + 172800);
									}
								});
							})
							.on('error', function(err) {
								expRes.json({ error: "Error contacting Cardcast API." });
							});
						}
					});
				})
				.on('error', function(err) {
					expRes.json({ error: "Error contacting Cardcast API." });
				});
			}
		});
	})
	.use('/admin', new express.Router()
		.use('/', function(req, res, next) {
			var credentials = auth(req);

			if(!credentials || credentials.name !== adminCredentials.username || sha1(credentials.pass) !== adminCredentials.password) {
				res.writeHead(401, {
					'WWW-Authenticate': 'Basic realm="Login"'
				});
				res.end();
			}
			else
				next();
		})
		.post('/refreshDefaultDecks', function(req, res) {
			_refreshDefaultDecks(function(err) {
				if(err)
					res.json({ error: err.toString() });
				else
					res.json({ success: true });
			});
		})
	)
	.use('/js', uglify({ sourcePath: 'web/js', type: 'js' }))
	.use('/css', uglify({ sourcePath: 'web/css', type: 'css' }))
	.use('/', express.static('web'));

io
	.use(require('./middleware/socket.io-cookie-bridge'));

io.on('connection', function(socket) {
	socket.on('error', function(err) {
		throw err;
	});

	socket.cookieBridge.get('session', function(session) {
		var ipua = sha1(socket.handshake.address + '/' + socket.handshake.headers['user-agent']);
		if(session) {
			session = cookie.unsign(session, secret);
			var user = users.findBy('session', session);
			if(user && session.split('+')[1] === ipua) {
				if(user.socket) {
					socket.once('take over', function() {
						// The disconnect event will set the kick timer (if user.game), prevent that.
						user.retainUserAfterDisconnection = true;

						if(user.socket) { // It can happen..
							user.socket.emit('take over');
							user.socket.disconnect();
						}
						
						user.socket = socket;

						if(user.game) {
							user.game.registerUserSocket(user);
							user.game.update();
						}
						user.emitGames(games);

						socket.emit('session ok');
						bind();
						user.update(true);
					});
					socket.emit('take over required');
					return;
				}

				user.socket = socket;

				if(user.kickTimeout !== undefined) {
					clearTimeout(user.kickTimeout);
					delete user.kickTimeout;
				}

				if(user.game) {
					user.game.registerUserSocket(user);
					user.game.update();
				}
				user.emitGames(games);

				socket.emit('session ok');
				bind();
				user.update(true);
				return;
			}
		}

		var id = uuid.v4();
		session = id + '+' + ipua;
		socket.cookieBridge.set('session', cookie.sign(session, secret));
		var user = new User;
		user.socket = socket;
		user.session = session;
		user.id = id;
		users.push(user);

		user.emitGames(games);
		socket.emit('session ok');
		bind();
		user.update();
	});

	function bind() {
		socket.on('set nickname', function(nickname) {
			if(!nickname || !/(?=^[\w ]{1,32}$)(?=^((?!  ).)*$)/.test(nickname))
				return console.log("Nickname invalid");

			if(users.isDuplicate('nickname', nickname))
				return socket.emit('notification', "Sorry, that nickname is taken.");

			var user = users.findBy('socket', socket);
			
			if(user.nickname)
				return;

			user.nickname = nickname;
			user.update();
		});

		socket.on('create game', function() {
			var user = users.findBy('socket', socket);
			if(user.game || !user.nickname)
				return;

			var game = new Game(io);
			games.push(game);
			game.add(user);
			io.emit('games', { incremental: { add: game.summarize() } });
		});

		socket.on('join game', function(data) {
			var user = users.findBy('socket', socket);
			if(user.game || !user.nickname)
				return;

			var game = games.findBy('id', data.id);
			if(!game)
				return socket.emit('notification', "This game doesn't exist.");

			if(game.settings.password) {
				if(data.password) {
					if(data.password === game.settings.password)
						game.add(user);
					else
						socket.emit('notification', "That password is incorrect.");
				}
			}
			else
				game.add(user);
		});

		socket.on('leave game', function() {
			var user = users.findBy('socket', socket);
			if(!user.game)
				return;

			user.game.kick(user, true); // intentional kick
		});

		socket.on('disconnect', function() {
			var user = users.findBy('socket', socket);
			if(!user)
				return console.log("user = null upon disconnect.. WTF?!");
			delete user.socket;

			if(!user.retainUserAfterDisconnection) {
				if(user.game) {
					user.game.update();
					user.kickTimeout = setTimeout(function() {
						if(user.game)
							user.game.kick(user);
					}, 60000);
				}
				else
					users.remove(user);
			}
			user.retainUserAfterDisconnection = false;
		});
	};
});

function _refreshDefaultDecks(callback) {
	function _processRawJSON (data) {
		var json = JSON.parse(data);
		if(!json || !json.results || json.results.count === 0)
			callback(new Error("Fetching default decks failed"));
		else {
			defaultDecksCodes = [];

			defaultDecks = json.results.data.map(function(deck) {
				defaultDecksCodes.push(deck.code);

				return {
					code: deck.code,
					name: deck.name.replace(' - CAH' , ''),
					callCount: deck.call_count * 1,
					responseCount: deck.response_count * 1,
					rating: deck.rating * 1,
					standard: true
				};
			});
			
			defaultDecks[0].selected = true;

			if(callback)
				callback();
		}
	};

	/*
	https.get('https://api.cardcastgame.com/v1/decks?author=CAH&direction=asc&limit=50&offset=0&sort=created_at', function(res) {
		res.setEncoding('utf8');
		var data = '';
		res.on('data', function(chunk) {
			data += chunk;
		});
		res.on('end', function() {
			_processRawJSON(data);
		});
	})
	.on('error', function(err) {
		if(callback)
			callback(err);
	});
	*/

	fs.readFile('defaultDecks.json', 'utf8', function(err, data) {
		if(err)
			callback(err);
		else
			_processRawJSON(data);
	});
};

function _getCardcastMetaData(code, callback, noCache) {
	db.get("SELECT * FROM decks WHERE code = ?", code, function(err, row) {
		if(err)
			callback(err);
		else {
			if(row) {
				row.rating /= 10;
				callback(null, row);
			}
			else {
				https.get('https://api.cardcastgame.com/v1/decks/' + code, function(res) {
					res.setEncoding('utf8');
					var data = '';
					res.on('data', function(chunk) {
						data += chunk;
					});
					res.on('end', function() {
						var json = JSON.parse(data);
						if(!json || !json.code)
							callback(new Error(json.message ? json.message : "Couldn't get deck metadata."));
						else {
							var deck = {
								code: json.code,
								name: json.name,
								description: json.description,
								callCount: json.call_count * 1,
								responseCount: json.response_count * 1,
								rating: json.rating * 1
							};

							if(noCache)
								callback(null, deck);
							else {
								db.run("INSERT INTO decks VALUES (?, ?, ?, ?, ?, ?)", deck.code, deck.name, deck.description, deck.callCount, deck.responseCount, deck.rating * 10, function(err) {
									if(err && err.errno !== 19)
										callback(err);
									else
										callback(null, deck);
								});
							}
						}
					});
				});
			}
		}
	});
};

var rdy = require('rdy')(3, function() {
	http.listen(2000);
});

_refreshDefaultDecks(rdy);

db = new sqlite3.Database(cardcastCacheFile, function() {
	db.parallelize();
	db.run("CREATE TABLE IF NOT EXISTS decks(code TEXT PRIMARY KEY, name TEXT, description TEXT, callCount INTEGER, responseCount INTEGER, rating INTEGER)", rdy);
	db.run("CREATE TABLE IF NOT EXISTS cards(code TEXT PRIMARY KEY, json TEXT, expires INTEGER)", rdy);
});
