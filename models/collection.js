(function() {
	var Collection = function(type) {
		this.type = type;
	};
	module.exports = Collection;
	Collection.prototype = [];
	Collection.prototype.isDuplicate = function(field, value) {
		return this.some(function(element) {
			if(element[field] === value || (typeof element[field] === 'string' && typeof value === 'string' && element[field].toLowerCase() === value.toLowerCase()))
				return true;

			return false;
		});
	};
	Collection.prototype.findBy = function(field, value) {
		var returnValue = false;
		if(this.some(function(element) {
			if(element[field] === value) {
				returnValue = element;
				return true;
			}

			return false;
		})) {
			return returnValue;
		}
	};
	var push = Collection.prototype.push;
	Collection.prototype.push = function() {
		var collection = this;
		if(this.type) {
			if(!Array.prototype.every.call(arguments, function(value) {
				if(value instanceof collection.type) {
					value._parentCollection = collection;
					return true;
				}
				return false;
			}))
				throw new Error("Incorrect type.");
		}
		
		return push.apply(this, arguments);
	};
	Collection.prototype.remove = function(element) {
		delete element._parentCollection;
		this.splice(this.indexOf(element), 1);
	};
})();