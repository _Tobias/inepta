(function() {
	var Collection = require('./collection');
	var User = require('./user');
	var diff = require('rus-diff').diff;
	var validation = require('../web/js/inepta.validation');

	var Game = function(io) {
		this.io = io;
		this.id = require('uuid').v4();
		this.users = new Collection(User);
		this.internal = {};
		this.settings = {
			decks: ['CAHBS'],
			idleTimer: true,
			playerLimit: 8,
			scoreLimit: 10,
			password: null,
			name: null
		};
		this.socketsReady = [];
	};
	module.exports = Game;

	Game.prototype = {
		io: null,
		id: null,
		users: null,
		czar: null,
		host: null,
		playing: false,
		settings: null,
		internal: null,
		socketsReady: null
	};

	Game.prototype.summarize = function() {
		this.internal.summary = {
			id: this.id,
			playerCount: this.users.length,
			playerLimit: this.settings.playerLimit,
			playing: this.playing,
			password: !!this.settings.password,
			host: this.host ? this.host.nickname : null,
			name: this.settings.name
		};

		return this.internal.summary;
	};

	Game.prototype.summarizeIncremental = function() {
		/**
		 * As soon as one of these change, a 'games' event should be
		 * emitted to all sockets with data in this format:
		 * { incremental: { update: this.summarizeIncremental() } }
		 */

		 var oldSummary = this.internal.summary;
		 var newSummary = this.summarize();

		 return { diff: diff(oldSummary, newSummary), id: this.id };
	};

	Game.prototype.kick = function(user, intentional) {
		var game = this;
		this.users.some(function(value, index) {
			if(value === user) {
				user.game = null;
				user.inGame = false;

				if(game.czar === user)
					game.czar = null;

				if(game.host === user)
					game.host = null;

				game.users.splice(index, 1);

				if(user.socket) {
					if(!intentional)
						user.socket.emit('notification', "You are no longer part of this game.");
					
					user.socket.leave('game ' + game.id);
					game._unregisterSocket(user.socket);
					user.update();
				}

				/**
				 * Exception: if game.users.length === 0, the game will be
				 * removed from the game list anyway by _resolveConflicts.
				**/

				if(game.users.length > 0)
					game._updateListing();

				game._resolveConflicts();
				return true;
			}
			return false;
		});
	};


	/**
	 * The noUpdate parameter is used when .start or .stop is called
	 * and .update is bound to be called later on.
	 */
	Game.prototype.start = function(noUpdate) {
		this.playing = true;

		if(!noUpdate)
			this.update();

		this._emit('load cards');

		this._updateListing();
	};

	Game.prototype.stop = function(noUpdate) {
		this.playing = false;

		if(!noUpdate)
			this.update();

		this._updateListing();
	};

	Game.prototype.add = function(user) {
		if(this.settings.playerLimit === this.users.length)
			return user.socket.emit('notification', "This game is full.");

		user.score = 0;
		user.czarCount = 0;
		user.game = this;
		user.inGame = true;

		var firstUser;

		if(this.users.length === 0) {
			this.host = user;
			firstUser = true; // Game has just been created by user
		}

		this.users.push(user);
		this.registerUserSocket(user);

		this.update();
		user.update(true);

		if(!firstUser)
			this._updateListing();
	};

	/**
	 * Function that registers all game-specific listeners
	 */
	Game.prototype.registerUserSocket = function(user) {
		var game = this;

		user.socket.join('game ' + this.id);

		user.socket.on('game settings', function(data) {
			if(user === game.host) {
				var results = validation('gameSettings', game.settings, data, game.settings);

				if(results.error)
					user.socket.emit('notification', results.error);

				if(results.changed.indexOf('playerLimit') > -1 || results.changed.indexOf('password') > -1 || results.changed.indexOf('name') > -1)
					game._updateListing();

				// volatile doesn't work with rooms, apparently
				game.io.to('game ' + game.id)./*volatile.*/emit('game state', { incremental: { update: { settings: game.settings } } });
			}
		});

		user.socket.on('game controls', function(data) {
			if(user === game.host) {
				if(data.action === 'start')
					game.start();
				else if(data.action === 'stop')
					game.stop();
			}
		});

		user.socket.on('cards loaded', function() {
			game.socketsReady.push(user.socket);

			if(this.socketsReady.length === this.users.length) {

			}
		});
	};

	Game.prototype._unregisterSocket = function(socket) {
		socket.removeAllListeners('game settings');
		socket.removeAllListeners('game controls');
		
		var index = this.socketsReady.indexOf(socket);
		if(index > -1)
			this.socketsReady.splice(index, 1);
	};

	Game.prototype.update = function(target) {
		var host = this.host;
		(target || this.io.to('game ' + this.id)).emit('game state', { all: {
			users: this.users.map(function(user) {
				return {
					id: user.id,
					score: user.score,
					nickname: user.nickname,
					alive: !!user.socket,
					host: user === host
				};
			}),
			czar: this.czar,
			settings: this.settings,
			playing: this.playing,
			id: this.id
		}});
	};

	Game.prototype._updateListing = function() {
		if(this.internal.summary) {
			var incrementalSummary = this.summarizeIncremental();
			if(incrementalSummary.diff)
				this.io.emit('games', { incremental: { update: incrementalSummary } });
		}
		else
			this.io.emit('games', { incremental: { update: this.summarize() } });
	};

	/**
	 * Called after a player is kicked. Should emit update.
	 */
	Game.prototype._resolveConflicts = function(listingUpdateRequired) {
		if(this.users.length === 0) {
			this._parentCollection.remove(this);
			this.io.emit('games', { incremental: { remove: this.id } });
			// Game is not accessed anymore from this point.
			return;
		}

		if(!this.host) {
			this.host = this.users[0];
			this._updateListing();
			this._emit('notification', this.host.nickname + " is the new host.");
		}
		if(this.playing) {
			if(this.users.length < 3) {
				this._emit('notification', "Not enough players to continue.");
				this.stop(true);
			}
			else if(!this.czar) {
				this.czar = this._selectCzar();
				this._emit('notification', this.czar.nickname + " is the new Card Czar.");
			}
		}
		this.update();
	};

	Game.prototype._selectCzar = function() {
		var candidate;
		this.users.some(function(user) {
			if(candidate === undefined || user.czarCount < candidate.czarCount) {
				candidate = user;
				return true;
			}
			return false;
		});
		candidate.czarCount++;
		return candidate;
	};

	Game.prototype._emit = function(event, what) {
		this.io.to('game ' + this.id).emit(event, what);
	};
})();