(function() {
	var User = function() {};
	module.exports = User;

	User.prototype = {
		id: null,
		session: null,
		socket: null,
		nickname: null,
		kickTimeout: null,
		game: null,
		retainUserAfterDisconnection: false
	};

	User.prototype.update = function(noGameUpdate) {
		if(this.game) {
			if(!noGameUpdate)
				this.game.update(this.socket);
		}
		
		this.socket.emit('user state', {
			inGame: !!this.game,
			nickname: this.nickname,
			id: this.id
		});
	};

	User.prototype.emitGames = function(games) {
		this.socket.emit('games', { all: games.map(function(game) { return game.summarize(); }) });
	};
})();