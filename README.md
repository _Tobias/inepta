# Game (game.js)
The `Game` object is an object that describes a room that people can join, and later play the game in.

## Game#constructor(io)
Constructs a `Game` object.

## Game#summarize()
`Game#summarize` creates an object with some information about the group, and returns an object with that information. It also stores the object into Game#internal.summary for caching purposes.

## Game#summarizeIncremental
`Game#summarizeIncremental` uses the object returned by `Game#summarize` and compares it with the old summary.

## Game#kick
`Game#kick` kicks a user from a game. This can be done by either the admin or themselves, as disconnection triggers a timeout for a kick.

