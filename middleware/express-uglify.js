module.exports = function(options) {
	var express = require('express');
	var ujs = require('uglify-js');
	var ucss = require('uglifycss');
	var fs = require('fs');
	require('string.prototype.endswith');

	options = options || {};
	var type = options.type || 'js';
	var minifiedSuffix = options.minifiedSuffix || '.minified.' + type;
	var sourcePath = options.sourcePath || '.';
	var rootPath = options.rootPath || __dirname;

	return express.Router()
	.use(function(req, res, next) {
		if(req.path.endsWith('/'))
			return next();
		var sourceFile = sourcePath + req.path;
		var minifiedFile = sourceFile + minifiedSuffix;

		if(req.path.endsWith('.min.' + type) || req.url.endsWith('?nomin'))
			next();
		else {
			fs.stat(sourceFile, function(err, sourceStats) {
				if(err) // source doesn't exist. serve source (404)
					return next();

				fs.stat(minifiedFile, function(err, minifiedStats) {
					if(err)
						minifyAndSend(sourceStats);
					else {
						if(Math.floor(sourceStats.mtime.getTime() / 1000) === Math.floor(minifiedStats.mtime.getTime() / 1000)) {
							req.url = req.path + minifiedSuffix;
							next();
						}
						else
							minifyAndSend(sourceStats);
					}
				});
			});
		}

		function minifyAndSend(sourceStats) {
			var result;
			try {
				result = type === 'js' ? ujs.minify(sourceFile) : ucss.processFiles([sourceFile]);
			}
			catch(e) {
				console.log("Failed to minify " + sourceFile);
				return next();
			}
			fs.writeFile(minifiedFile, type === 'js' ? result.code : result, function(err) {
				fs.utimes(minifiedFile, sourceStats.atime, sourceStats.mtime, function(err) {
					req.url = req.path + minifiedSuffix;
					next();
				});
			});
		}
	})
	.use(express.static(sourcePath));
};