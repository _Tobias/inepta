module.exports = function(socket, next) {
	if(!socket.cookieBridge) {
		var uuid = require('uuid');
		var callbacks = {};

		socket.cookieBridge = {
			get: function(name, callback) {
				var request = uuid.v4();
				callbacks[request] = callback;
				socket.emit('get cookie', { name: name, request: request });
			},

			set: function(name, value, expires) {
				socket.emit('set cookie', { name: name, value: value, expires: expires });
			},

			expire: function(name) {
				socket.emit('expire cookie', { name: name });
			}
		};

		socket.on('get cookie', function(data) {
			if(data.request && callbacks[data.request]) {
				callbacks[data.request](data.value);
				delete callbacks[data.request];
			}
		});
	}

	next();
}