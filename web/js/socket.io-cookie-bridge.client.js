(function() {
	io.Socket.prototype.cookieBridge = function(fakeCookies) {
		this.on('set cookie', function(data) {
			if(fakeCookies && fakeCookies[data.name] !== undefined)
				fakeCookies[data.name] = data.value;
			else
				Cookies.set(data.name, data.value, { expires: data.expires || Infinity });
		});
		this.on('get cookie', function(data) {
			if(fakeCookies && fakeCookies[data.name] !== undefined)
				this.emit('get cookie', { value: fakeCookies[data.name], request: data.request });
			else
				this.emit('get cookie', { value: Cookies.get(data.name), request: data.request });
		});
		this.on('expire cookie', function(data) {
			if(fakeCookies && fakeCookies[data.name] !== undefined)
				fakeCookies[data.name] = null;
			else
				Cookies.expire(data.name);
		});

		return this;
	};
})();