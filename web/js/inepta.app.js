(function() {
	var socket = io().cookieBridge(),
		gameState = {},
		userState = {},
		loadingCards = false,
		cards = {};

	window.socket = socket;

	socket.on('session ok', function() {
		_hideOverlay('preparing');
		_hideOverlay('take-over');
	});

	socket.on('game state', function(data) {
		if(data.all)
			gameState = data.all;
		else if(data.incremental && data.incremental.update)
			Object.extend(gameState, data.incremental.update);

		_reflectGameState();
	});

	socket.on('user state', function(data) {
		userState = data;

		if(userState.nickname) {
			_hideOverlay('nickname');
			$('#nickname-indicator').text(userState.nickname);
		}
		else
			_showOverlay('nickname');

		var sections = $('#content > div').hide();
		if(userState.inGame && gameState.playing)
			sections.filter('#game-arena').show();
		else if(userState.inGame) {
			// User MIGHT just have joined this game.
			_hideOverlay('password');
			sections.filter('#game-lobby').show();
		}
		else
			sections.filter('#game-list').show();

		$('#game-settings-button')[userState.inGame && gameState.playing ? 'show' : 'hide']();

		$('#create-leave-button').text(data.inGame ? 'Leave game' : 'Create a game');

		_reflectHost();
	});

	var games = [];
	socket.on('games', function(data) {
		var gameList = document.getElementById('game-list');

		if(data.all) {
			games = data.all;
			gameList.innerHTML = '';
			games.forEach(function(game) {
				gameList.appendChild(_gameToNode(game));
			});
		}
		else if(data.incremental) {
			if(data.incremental.add) {
				games.push(data.incremental.add);
				gameList.appendChild(_gameToNode(data.incremental.add));
			}
			else if(data.incremental.update) {
				games.some(function(game, index) {
					if(game.id === data.incremental.update.id) {
						if(data.incremental.update.diff) {
							RusDiff.apply(game, data.incremental.update.diff);
						}
						else
							games[index] = data.incremental.update;

						$('[data-game="' + games[index].id + '"]').replaceWith(_gameToNode(games[index]));
						return true;
					}
					return false;
				});
			}
			else if(data.incremental.remove) {
				games.some(function(game, index) {
					if(game.id === data.incremental.remove) {
						games.splice(index, 1);
						$('[data-game="' + game.id + '"]').remove();
						return true;
					}
					return false;
				});
			}
		}

		console.log('games list updated', games);
	});

	socket.on('take over required', function() {
		_hideOverlay('preparing');
		_showOverlay('take-over')
			.find('.yeah')
				.off('click')
				.click(function() {
					socket.emit('take over');
				});
	});

	socket.on('take over', function(data) {
		_showOverlay('taken-over');
	});

	socket.on('load cards', function(data) {
		if(!loadingCards) {
			loadingCards = true;
			var index = 0;
			var deck = gameState.settings.decks[index];

			function loop() {
				if(!deck) {
					loadingCards = false;
					socket.emit('cards loaded');
					return;
				}

				if(!cards[deck]) {
					ajax.cards(deck, function(data) {
						if(data.error)
							showNotification(data.error);
						else {
							cards[deck] = data;
							deck = gameState.settings.decks[++index];
							loop();
						}
					});
				}
				else {
					deck = gameState.settings.decks[++index];
					loop();
				}
			};
			loop();
		}
	});

	$(function() {
		var nickname = document.getElementById('nickname');
		nickname.addEventListener('keydown', _validateInput.bind(nickname, 'nickname', function() {
			socket.emit('set nickname', this.value.trim());
			this.value = '';
		}, /(?=^[\w ]{1,32}$)(?=^((?!  ).)*$)/));

		$('#create-leave-button').click(function() {
			if(userState.inGame)
				_showOverlay('leave-confirmation');
			else
				socket.emit('create game');
		});

		$('#leave-confirmation-overlay .inepta-button')
			.click(function() {
				_hideOverlay('leave-confirmation');
			})
			.filter('.yeah')
				.click(function() {
					socket.emit('leave game');
				});

		$('#password-overlay .inepta-button')
			.click(function() {
				$(this).parent().find('input').val('');
				_hideOverlay('password');
			});

		$('#add-from-cardcast-button').click(function() {
			_showOverlay('cardcast');
		});

		var cardcastCode = $('#cardcast-code');
		cardcastCode.on('keydown _submit', _validateInput.bind(cardcastCode[0], 'Cardcast code', function() {
			this.focus();
			if($('[data-cardcast="' + this.value + '"]').length > 0) {
				showNotification("This deck is already added.");
				this.value = '';
				return;
			}
			// cardcast should be placed behind cardcast-loading
			_showOverlay('cardcast-loading', true);
			ajax.deck(this.value, function(deck) {
				_hideOverlay('cardcast-loading');
				if(deck.error) {
					showNotification(deck.error);
					cardcastCode.focus();
				}
				else {
					cardcastCode.val('');
					deck.selected = true;
					$('#game-settings-available-decks').append(_deckToNode(deck));
					_hideOverlay('cardcast', true);
					_syncCardDecks();
				}
			});
		}, /^[A-Z0-9]{5}$/));

		$('#cardcast-overlay .inepta-button:not(.add)').click(function() {
			_hideOverlay('cardcast');
		});

		$('#cardcast-overlay .inepta-button.add').click(function() {
			cardcastCode.trigger('_submit');
		});

		$.getJSON('/decks/default', function(decks) {
			var wrapper = $('#game-settings-available-decks');
			decks.forEach(function(deck) {
				wrapper.append(_deckToNode(deck));
			});

			wrapper.find('.deck').addClass('standard').click(function() {
				$(this).toggleClass('selected');
				_syncCardDecks();
			});
		});

		var scoreLimit = $('#game-settings-score-limit');
		var playerLimit = $('#game-settings-player-limit');
		for(var i = 3; i <= 100; i++) {
			scoreLimit.append('<option value="' + i + '">' + i + ' points' + '</option>');
			if(i <= 20)
				playerLimit.append('<option value="' + i + '">' + i + ' players' + '</option>');
		}

		$('#game-settings-password-show').click(function() {
			var password = $('#game-settings-password');
			var isHidden = password.attr('type') === 'password';
			password.attr('type', isHidden ? 'text' : 'password');
			$(this)[isHidden ? 'addClass' : 'removeClass']('visible');
		});

		$('#game-settings input, #game-settings select').on('keydown change', function(event) {
			$('#game-settings-apply, #game-settings-revert').show();
		});

		$('#game-settings-apply').click(function() {
			var draft = {
				name: $('#game-settings-name').val(),
				password: $('#game-settings-password').val(),
				scoreLimit: $('#game-settings-score-limit').val() * 1,
				playerLimit: $('#game-settings-player-limit').val() * 1,
				idleTimer: $('#game-settings-idle-timer').val() === 'on'
			};

			var proposal = {};

			var results = IneptaValidator('gameSettings', gameState.settings, draft, proposal);
			if(results.error)
				showNotification(results.error);

			if(results.changed.length > 0)
				socket.emit('game settings', proposal);

			$('#game-settings-apply, #game-settings-revert').hide();
		});

		$('#game-settings-revert').click(function() {
			_reflectGameState();
			$('#game-settings-apply, #game-settings-revert').hide();
		});

		// Browser flaw check
		!function() {
			var flawless = true;
			var ok = true;
			var flaws = ['?rgba', 'opacity', '?cssanimations', '?csstransitions', 'localstorage'];
			var flawCaptions = ['RGBA', 'Opacity', 'CSS Animations', 'CSS Transitions', 'Local storage'];
			flaws.forEach(function(key, index) {
				var optional = key.indexOf('?') === 0;
				if(!Modernizr[optional ? key.substr(1) : key]) {
					flawless = false;
					if(!optional)
						ok = false;

					$('#browser-flaws-list').append('<br>- ' + flawCaptions[index]);
				}
			});

			if(ok && Cookies.get('dontcareaboutbrowserflaws') === 'yeah')
				return;

			if(!flawless) {
				var continueButton = _showOverlay('browser-flaws')
					.find('.inepta-button:not(.yeah)').click(function() {
						Cookies.set('dontcareaboutbrowserflaws', 'yeah', { expires: 1209600 }); // 14 days
						_hideOverlay('browser-flaws');
					});
				if(ok)
					continueButton[0].removeAttribute('style');
				else
					socket.disconnect();
			}
		}();
	});

	var notificationHideTimeout;
	function showNotification(data) {
		clearTimeout(notificationHideTimeout);

		var text;
		var duration = 3000;
		if(typeof data === 'string')
			text = data;
		else {
			text = data.text;
			duration = data.duration;
		}

		var notificationBar = $('#notification-bar');
		notificationBar
			.stop()
			.html(text)
			.css({ top: -notificationBar.height() })
			.transition({ top: 0 });

		notificationHideTimeout = setTimeout(function() {
			notificationBar.transition({ top: -notificationBar.height() });
		}, duration);
	}
	socket.on('notification', showNotification);

	function _reflectGameState() {
		$('#game-settings-name').val(gameState.settings.name);
		$('#game-settings-password').val(gameState.settings.password);

		var selectedDecks = gameState.settings.decks;

		var wrapper = $('#game-settings-available-decks');

		var decks = wrapper.find('.deck').removeClass('selected');
		selectedDecks.forEach(function(deck) {
			var node = decks.filter('[data-cardcast="' + deck + '"]');
			if(node.length !== 0)
				node.addClass('selected');
			else {
				wrapper.append('<span data-cardcast-placeholder="' + deck + '"></span>');
				ajax.deck(deck, function(data) {
					if(data.error)
						showNotification("Error fetching metadata for deck " + deck + ".");
					else {
						data.selected = selectedDecks.indexOf(deck) > -1;
						$('[data-cardcast-placeholder="' + deck + '"]').replaceWith(_deckToNode(data));
					}
				});
			}
		});

		// Remove unselected non-standard decks
		decks.filter('.deck:not(.standard):not(.selected)').remove();

		$('#game-settings-score-limit').val(gameState.settings.scoreLimit);
		$('#game-settings-player-limit').val(gameState.settings.playerLimit);
		$('#game-settings-idle-timer').val(gameState.settings.idleTimer ? 'on' : 'off');

		_reflectHost();

		console.log('gameState updated', gameState);
	};

	function _reflectHost() {
		if(_amIHost()) {
			$('#game-lobby').find('input, select').removeAttr('disabled');
			$('#game-settings-decks-overlay').hide();
			$('#add-from-cardcast-button').show();
		}
		else {
			$('#game-lobby').find('input, select').attr('disabled', 'true');
			$('#game-settings-decks-overlay').show();
			$('#add-from-cardcast-button').hide();
		}
	};

	function _syncCardDecks() {
		var decks = [];
		$('.deck.selected').each(function(k, v) {
			decks.push($(this).attr('data-cardcast'));
		});
		socket.emit('game settings', { decks: decks });
	};

	function _makeIterator(array) {
		var nextIndex = 0;

		return {
			next: function() {
				return nextIndex < array.length ? array[nextIndex++] : null;
			},
			hasNext: function() {
				return nextIndex < array.length + 1;
			}
		};
	};

	function _validateInput(fieldName, callback, regex, event) {
		if(event.type === 'keydown' && event.which !== 13)
			return;

		var field = this;
		var value = field.value.trim();
		var valid = regex.test(value);
		if(field.checkValidity() && valid)
			callback.call(field);
		else {
			var what = 'is invalid';
			if(field.validity.valueMissing)
				what = 'is not specified';
			else if(field.validity.tooLong)
				what = 'is too long';
			else if(!valid)
				what = 'contains illegal character(s)';
			showNotification("Your " + fieldName + " " + what + ".");
		}
	};

	function _amIHost() {
		return gameState.users && gameState.users.some(function(user) {
			return user.id === userState.id && user.host;
		});
	};

	function _plural(count, singular, plural) {
		if(count === 1)
			return singular;
		else
			return plural;
	};

	function _ajaxBase(storagePrefix, restTemplate, deck, callback) {
		var cached = localStorage.getItem(storagePrefix + deck);
		if(cached)
			callback(JSON.parse(cached));
		else {
			$.get(restTemplate.replace('%', deck), function(data) {
				localStorage.setItem(storagePrefix + deck, data);
				callback(JSON.parse(data));
			}, 'text');
		}
	};

	var ajax = {
		deck: _ajaxBase.bind(this, 'deck_meta_', '/decks/%'),
		cards: _ajaxBase.bind(this, 'cards_', '/decks/%/cards')
	};

	// Overlays
	var overlays = {
		preparing: true
	};

	function _hideOverlay(selector, directly) {
		if(!overlays[selector])
			return $(selector);

		overlays[selector] = false;

		selector = $('#' + selector + '-overlay');

		if(directly)
			return selector.css({ opacity: 0 }).hide();

		return selector.stop().transition({ opacity: 0 }, 250, 'easeInCubic', function() {
			$(this).hide();
		});
	};

	function _showOverlay(selector, directly) {
		if(overlays[selector])
			return $(selector);

		overlays[selector] = true;

		selector = $('#' + selector + '-overlay');

		selector
			.stop()
			.show()
			.find('input')
				.eq(0)
				.focus();

		if(directly)
			return selector.css({ opacity: 1 });

		return selector.transition({ opacity: 1 }, 250, 'easeInCubic');
	};

	function _deckToNode(deck) {
		var node = document.getElementById('deck-template').cloneNode(true);
		node.removeAttribute('id');
		node.setAttribute('data-cardcast', deck.code);
		if(deck.description)
			node.title = deck.description;
		if(deck.selected)
			node.className += ' selected';

		if(!deck.standard) {
			node.addEventListener('click', function() {
				if(confirm("Do you want to remove this card deck?"))
					$(this).fadeOut(function() {
						$(this).remove();
						_syncCardDecks();
					});
			});
		}

		function child(key) {
			return node.getElementsByClassName('deck-' + key)[0];
		};

		child('name').innerText = deck.name;
		child('rating').innerText = deck.rating;
		child('card-count').innerText = deck.responseCount + deck.callCount;

		return node;
	};

	function _gameToNode(game) {
		var node = document.getElementById('game-list-entry-template').cloneNode(true);
		node.removeAttribute('id');
		node.setAttribute('data-game', game.id);

		function child(key) {
			return node.getElementsByClassName('game-list-entry-' + key)[0];
		};

		child('title').innerText = game.name || (game.host + "'s game");
		child('settings').innerText = game.playerCount + ' ' + _plural(game.playerCount, 'player', 'players') + ', ' + game.playerLimit + ' max, ' + (game.playing ? 'currently playing, ' : '') + (game.password ? 'password-protected' : 'public');
		child('join').addEventListener('click', function() {
			if(game.password) {
				var password = _showOverlay('password').find('#password').off('keydown');
				password.keydown(_validateInput.bind(password[0], 'password', function() {
					socket.emit('join game', { id: game.id, password: this.value });
					this.value = '';
				}, /^.{1,32}$/));
			}
			else
				socket.emit('join game', { id: game.id });
		});

		return node;
	};
})();