(function() {
	var IneptaValidator = (function() {
		function domain(b, value) {
			return typeof value === 'number' && value >= this && value <= b;
		};

		function type(value) {
			return typeof value === this.toString();
		};

		function regex(value) {
			return this.test(value);
		};

		function _arraysEqual(a, b) {
			if(a === b)
				return true;
			if(a === null || b === null)
				return false;
			if(a.length !== b.length)
				return false;

			for(var i = 0; i < a.length; ++i) {
				if(a[i] !== b[i])
					return false;
			}
			return true;
		}

		var sets = {
			gameSettings: {
				scoreLimit: domain.bind(3, 100),
				playerLimit: domain.bind(3, 20),
				idleTimer: type.bind('boolean'),
				password: regex.bind(/^.{1,32}$/),
				name: regex.bind(/(?=^[\w ]{1,32}$)(?=^((?!  ).)*$)/),
				decks: function(array) {
					if(!array instanceof Array)
						return;

					var regex = /^[A-Z0-9]{5}$/;

					return array.every(function(deck) {
						return regex.test(deck);
					});
				}
			}
		};

		return function(set, old, data, target) {
			var failed = [];
			var changed = [];

			var validation = sets[set];

			for(var key in validation) {
				if(validation.hasOwnProperty(key) && data[key] !== undefined && !(typeof data[key] === 'string' && data[key].length === 0)) {
					if(data[key] === old[key] || (old[key] instanceof Array && data[key] instanceof Array && _arraysEqual(old[key], data[key])))
						continue;

					if(validation[key](data[key])) {
						target[key] = data[key];
						changed.push(key);
					}
					else {
						console.log(key, [!(typeof data[key] !== 'object' && data[key] === old[key]), !(old[key] instanceof Array && data[key] instanceof Array && _arraysEqual(old[key], data[key])), validation[key](data[key])]);
						failed.push(key);
					}
				}
			}

			var error = null;
			if(failed.length > 0) {
				if(failed.length === 1)
					error = "Field <i>" + failed[0] + "</i> couldn't be saved.";
				else
					error = "Fields <i>" + failed.join(', ') + "</i> couldn't be saved.";
			}

			return {
				failed: failed,
				changed: changed,
				error: error
			};
		};
	})();

	if(typeof module !== 'undefined')
		module.exports = IneptaValidator;
	if(typeof window !== 'undefined')
		window.IneptaValidator = IneptaValidator;
})();